package com.RestApi.lollocaffe.service;

import java.util.Collection;
import java.util.Optional;

import com.RestApi.lollocaffe.dto.OrderDto;
import com.RestApi.lollocaffe.entity.Order;

public interface OrderService {

	public Order saveOrder(OrderDto orderIn);

	public Optional<Order> searchById(Long id);

	public void deleteById(Long id);

	public Collection<Order> findAll();

}
