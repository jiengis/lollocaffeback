package com.RestApi.lollocaffe.service;

import java.util.Collection;
import java.util.Optional;

import com.RestApi.lollocaffe.dto.CustomerDto;
import com.RestApi.lollocaffe.entity.Customer;

public interface CustomerService {

	public Customer saveCustomer(CustomerDto customerIn);

	public Optional<Customer> searchById(Long id);

	public void deleteById(Long id);

	public Collection<Customer> findAll();
}
