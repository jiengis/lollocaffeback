package com.RestApi.lollocaffe.service;

import java.util.Collection;
import java.util.Optional;

import com.RestApi.lollocaffe.dto.CoffeeDto;
import com.RestApi.lollocaffe.entity.Coffee;

public interface CoffeeService {

	public Coffee saveCoffee(CoffeeDto coffeeIn);

	public Optional<Coffee> searchById(Long id);

	public void deleteById(Long id);

	public Collection<Coffee> findAll();
}
