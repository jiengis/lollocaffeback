package com.RestApi.lollocaffe.service.impl;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.RestApi.lollocaffe.dto.OrderDto;
import com.RestApi.lollocaffe.entity.Order;
import com.RestApi.lollocaffe.repository.CoffeeRepository;
import com.RestApi.lollocaffe.repository.OrderRepository;
import com.RestApi.lollocaffe.service.OrderService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
public class OrderServiceImpl implements OrderService {

	private final OrderRepository orderRepository;
	private final CoffeeRepository coffeeRepository;

	@Override
	public Order saveOrder(OrderDto orderIn) {
		Order order = new Order();
		order.setRicezione(new Date());
		order.setCorriere(orderIn.getCorriere());
		order.setStato(orderIn.getStato());
		order.setChiusura(orderIn.getChiusura());
		order.setAccepted(orderIn.isAccepted());
		order.setCoffeeOrdered(coffeeRepository.findAllById(orderIn.getCoffeeOrdered()));
		return orderRepository.save(order);
	}

	@Override
	public Optional<Order> searchById(Long id) {
		return orderRepository.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		orderRepository.deleteById(id);
	}

	@Override
	public Collection<Order> findAll() {
		return orderRepository.findAll();
	}

}
