package com.RestApi.lollocaffe.service.impl;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.RestApi.lollocaffe.dto.CoffeeDto;
import com.RestApi.lollocaffe.entity.Coffee;
import com.RestApi.lollocaffe.repository.CoffeeRepository;
import com.RestApi.lollocaffe.service.CoffeeService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
public class CoffeeServiceImpl implements CoffeeService {

	private final CoffeeRepository coffeeRepository;

	@Override
	public Coffee saveCoffee(CoffeeDto coffeeIn) {
		Coffee coffee = new Coffee();
		coffee.setBrand(coffeeIn.getBrand());
		coffee.setCategory(coffeeIn.getCategory());
		coffee.setMiscela(coffeeIn.getMiscela());
		coffee.setPrice(coffeeIn.getPrice());
		coffee.setTitle(coffeeIn.getTitle());
		coffee.setOrderNumber(null);
		return coffeeRepository.save(coffee);
	}

	@Override
	public Optional<Coffee> searchById(Long id) {
		return coffeeRepository.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		coffeeRepository.deleteById(id);
	}

	@Override
	public Collection<Coffee> findAll() {
		return coffeeRepository.findAll();
	}

}
