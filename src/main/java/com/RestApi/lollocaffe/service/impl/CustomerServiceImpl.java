package com.RestApi.lollocaffe.service.impl;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.RestApi.lollocaffe.dto.CustomerDto;
import com.RestApi.lollocaffe.entity.Customer;
import com.RestApi.lollocaffe.repository.CustomerRepository;
import com.RestApi.lollocaffe.service.CustomerService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor(onConstructor = @__({ @Autowired }))
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;

	@Override
	public Customer saveCustomer(CustomerDto customerIn) {
		Customer customer = new Customer();
		customer.setFirstname(customerIn.getFirstname());
		customer.setLastname(customerIn.getLastname());
		customer.setEmail(customerIn.getEmail());
		customer.setPhone(customerIn.getPhone());
		customer.setPassword(customerIn.getPassword());
		customer.setPrivileges(customerIn.getPrivileges());
		customer.setOrders(null);
		return customerRepository.save(customer);
	}

	@Override
	public Optional<Customer> searchById(Long id) {
		return customerRepository.findById(id);
	}

	@Override
	public void deleteById(Long id) {
		customerRepository.deleteById(id);
	}

	@Override
	public Collection<Customer> findAll() {
		return customerRepository.findAll();
	}

}
