package com.RestApi.lollocaffe.helper;

import com.RestApi.lollocaffe.dto.CoffeeDto;
import com.RestApi.lollocaffe.entity.Coffee;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class CoffeeHelper {

    public static CoffeeDto convert(Coffee coffee) {
        CoffeeDto coffeeDto = new CoffeeDto();
        coffeeDto.setId(coffee.getId());
        coffeeDto.setCategory(coffee.getCategory());
        coffeeDto.setTitle(coffee.getTitle());
        coffeeDto.setBrand(coffee.getBrand());
        coffeeDto.setPrice(coffee.getPrice());
        coffeeDto.setMiscela(coffee.getMiscela());
        coffeeDto.setOrderNumber(coffee.getOrderNumber().getId());
        return coffeeDto;
    }


    public static Collection<CoffeeDto> convert(Collection<Coffee> all){
        return all.stream()
                .map(CoffeeHelper::convert)
                .collect(Collectors.toList());

    }
}
