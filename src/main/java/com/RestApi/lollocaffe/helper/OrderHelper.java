package com.RestApi.lollocaffe.helper;

import com.RestApi.lollocaffe.dto.OrderDto;
import com.RestApi.lollocaffe.entity.Order;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class OrderHelper {

    public static OrderDto convert(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setBuyer(order.getBuyer().getId());
        orderDto.setCoffeeOrdered(order.getCoffeeOrdered().stream().map(c -> c.getId()).collect(Collectors.toList()));
        orderDto.setCorriere(order.getCorriere());
        orderDto.setRicezione(order.getRicezione());
        orderDto.setAccepted(order.isAccepted());
        orderDto.setStato(order.getStato());
        orderDto.setChiusura(order.getChiusura());
        return orderDto;
    }

    public static Collection<OrderDto> convert(Collection<Order> all){
        return all.stream()
                .map(OrderHelper::convert)
                .collect(Collectors.toList());
    }
}
