package com.RestApi.lollocaffe.helper;

import com.RestApi.lollocaffe.dto.CustomerDto;
import com.RestApi.lollocaffe.entity.Customer;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.stream.Collectors;

@Component
public class CustomerHelper {

    public static CustomerDto convert(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setFirstname(customer.getFirstname());
        customerDto.setLastname(customer.getLastname());
        customerDto.setEmail(customer.getEmail());
        customerDto.setPhone(customer.getPhone());
        customerDto.setPassword(customer.getPassword());
        customerDto.setPrivileges(customer.getPrivileges());
        customerDto.setOrders(null);
        return customerDto;
    }

    public static Collection<CustomerDto> convert(Collection<Customer> all){
        return all.stream().map(CustomerHelper::convert).collect(Collectors.toList());
    }
}
