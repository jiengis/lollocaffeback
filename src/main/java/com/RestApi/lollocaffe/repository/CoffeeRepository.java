package com.RestApi.lollocaffe.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.RestApi.lollocaffe.entity.Coffee;

public interface CoffeeRepository extends JpaRepository<Coffee, Long>{

}
