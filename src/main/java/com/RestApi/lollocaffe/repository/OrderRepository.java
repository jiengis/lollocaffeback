package com.RestApi.lollocaffe.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.RestApi.lollocaffe.entity.Order;

public interface OrderRepository extends JpaRepository<Order, Long>{

}
