package com.RestApi.lollocaffe.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.RestApi.lollocaffe.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
