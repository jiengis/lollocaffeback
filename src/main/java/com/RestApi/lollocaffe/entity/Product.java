package com.RestApi.lollocaffe.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class Product {

	@Id
	@GeneratedValue
	private Long id;
	
	private String category;
	private String title;
	private String brand;
	private double price;
}
