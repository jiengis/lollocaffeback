
package com.RestApi.lollocaffe.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Customer {

	@Id
	@GeneratedValue
	private Long id;

	private String firstname;
	private String lastname;
	private String email;
	private Long phone;
	private String password;
	private String privileges;

	@OneToMany(mappedBy = "buyer")
	private Collection<Order> orders;
}
