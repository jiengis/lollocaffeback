package com.RestApi.lollocaffe.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper=true)
public class Coffee extends Product{

	private String miscela;
	
	@ManyToOne
	private Order orderNumber;
}
