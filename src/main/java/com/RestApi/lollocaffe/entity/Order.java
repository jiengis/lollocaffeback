package com.RestApi.lollocaffe.entity;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "ordini")
public class Order {

	@Id
	@GeneratedValue
	private Long id;

	private Date ricezione;
	private String corriere;
	private boolean accepted;
	private String stato;
	private Date chiusura;

	@ManyToOne
	private Customer buyer;

	@OneToMany(mappedBy = "orderNumber")
	private Collection<Coffee> coffeeOrdered;
}
