package com.RestApi.lollocaffe.controller;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import com.RestApi.lollocaffe.helper.CustomerHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.RestApi.lollocaffe.dto.CustomerDto;
import com.RestApi.lollocaffe.entity.Customer;
import com.RestApi.lollocaffe.service.CustomerService;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("customer")
@RestController
@RequiredArgsConstructor
public class CustomerController {

	private final CustomerService customerService;

	@PostMapping("create")
	public CustomerDto newCustomer(@RequestBody CustomerDto customerIn) {
		customerService.saveCustomer(customerIn);
		return customerIn;
	}
	
	@GetMapping("{id}")
	public ResponseEntity<?> searchById(@PathVariable Long id){
		Optional<Customer> optCustomer = customerService.searchById(id);
		if(optCustomer.isPresent()) {
			return ResponseEntity.ok(CustomerHelper.convert(optCustomer.get()));
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> deleteById(@PathVariable Long id){
		Optional<Customer> optCustomer = customerService.searchById(id);
		if(optCustomer.isPresent()) {
			customerService.deleteById(id);
			return ResponseEntity.ok(HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@GetMapping("getAll")
	public Collection<CustomerDto> findAll(){
		return customerService.findAll().stream().map(CustomerHelper::convert).collect(Collectors.toList());
	}
	
}
