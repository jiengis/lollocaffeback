package com.RestApi.lollocaffe.controller;

import java.util.Collection;
import java.util.Optional;

import com.RestApi.lollocaffe.helper.CoffeeHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.RestApi.lollocaffe.dto.CoffeeDto;
import com.RestApi.lollocaffe.entity.Coffee;
import com.RestApi.lollocaffe.service.CoffeeService;

@CrossOrigin
@RequestMapping("coffee")
@RestController
@RequiredArgsConstructor
public class CoffeeController {

	private final CoffeeService coffeeService;

	@PostMapping("create")
	public CoffeeDto newCoffee(@RequestBody CoffeeDto coffeeIn) {
		coffeeService.saveCoffee(coffeeIn);
		return coffeeIn;
	}
	
	@GetMapping("{id}")
	public ResponseEntity<?> searchById(@PathVariable Long id){
		Optional<Coffee> optCoffee = coffeeService.searchById(id);
		if(optCoffee.isPresent()) {
			return ResponseEntity.ok(CoffeeHelper.convert(optCoffee.get()));
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> deleteById(@PathVariable Long id){
		Optional<Coffee> optCoffee = coffeeService.searchById(id);
		if(optCoffee.isPresent()) {
			coffeeService.deleteById(id);
			return ResponseEntity.ok(HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("getAll")
	public Collection<Coffee> findAll(){
		return coffeeService.findAll();
	}

}








