package com.RestApi.lollocaffe.controller;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import com.RestApi.lollocaffe.helper.OrderHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.RestApi.lollocaffe.dto.OrderDto;
import com.RestApi.lollocaffe.entity.Order;
import com.RestApi.lollocaffe.service.OrderService;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("order")
@RestController
@RequiredArgsConstructor
public class OrderController {

	private final OrderService orderService;

	@PostMapping("create")
	public OrderDto newOrder(@RequestBody OrderDto orderIn) {
		orderService.saveOrder(orderIn);
		return orderIn;
	}
	
	@GetMapping("{id}")
	public ResponseEntity<?> getOrder(@PathVariable Long id){
		Optional<Order> optOrder= orderService.searchById(id);
		if(optOrder.isPresent()) {
			return ResponseEntity.ok(OrderHelper.convert(optOrder.get()));
		}else {
			return ResponseEntity.notFound().build();			
		}
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<?> deleteById(@PathVariable Long id){
		Optional<Order> optOrder = orderService.searchById(id);
		if(optOrder.isPresent()) {
			orderService.deleteById(id);
			return ResponseEntity.ok(HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@GetMapping("getAll")
	public Collection<OrderDto> findAll(){
		return orderService.findAll().stream().map(OrderHelper::convert).collect(Collectors.toList());
	}
}
