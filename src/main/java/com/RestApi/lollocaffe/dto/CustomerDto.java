package com.RestApi.lollocaffe.dto;

import java.util.Collection;
import java.util.stream.Collectors;

import com.RestApi.lollocaffe.entity.Customer;

import lombok.Data;

@Data
public class CustomerDto {

	private Long id;
	private String firstname;
	private String lastname;
	private String email;
	private Long phone;
	private String password;
	private String privileges;
	private Collection<Long> orders;

}




