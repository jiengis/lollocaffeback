package com.RestApi.lollocaffe.dto;

import lombok.Data;

@Data
public class CoffeeDto {

	private Long id;
	private String category;
	private String title;
	private String brand;
	private double price;
	private String miscela;
	private Long orderNumber;
	

}
