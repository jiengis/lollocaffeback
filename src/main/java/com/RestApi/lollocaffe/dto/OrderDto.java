package com.RestApi.lollocaffe.dto;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import com.RestApi.lollocaffe.entity.Order;

import lombok.Data;

@Data
public class OrderDto {

	
	private Long id;
	private Long buyer;
	private Collection<Long> coffeeOrdered;
	private String corriere;
	private Date ricezione;
	private boolean accepted;
	private String stato;
	private Date chiusura;
	

}
