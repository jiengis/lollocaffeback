package com.RestApi.lollocaffe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LollocaffeApplication {

	public static void main(String[] args) {
		SpringApplication.run(LollocaffeApplication.class, args);
	}

}

